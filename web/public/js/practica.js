/*!
* Este archivo es parte del proyecto practica
* 
* practica ========
* 
* Practicando
* 
* @copyright Copyright (c);
* 
* @filesource
* 
* Hecho con <3 por Glendy
* 
* DEPENDENCIES:
* JQUERY
* Font Awesome
* 
* 
*/
'use strict';

var globalElements = {}; //Json de elementos

(function () {
    /**
     * Definimos los elementos que vamos a utilizar de la aplicación
     */
    this.getElements = function () {
        return {
            contenedor: {
                contenedorPrincipal: '#contedor_principal'
            },
            inputs: {
                inputActividad: '#input-prueba'
            },
            buttons: {
                buttonInsertReactivo: '#btn-insertReac'
            },
            tabla: {
                tbVista: '#example'
            }
        };
    };
}).apply(globalElements); //Aplicando todo los elementos al globalElements
//crea la funcion get Ajax para que podamos utilizar para no perer la info. de clientes y agencias
// function getAjax(ruta, fnSuccess)
// {   
//     console.log(ruta);
//     $.ajax({
//       url: ruta
//     }).done(function(data) {
//         console.log('Bien');
//     });
// } 
// 

// Namespace del módulo
// var ajax = {};

// (function($, _) {
//    this.saludarAjax = function(){
//      console.log('ajax');
//    }
// }).apply(ajax, [jQuery], _);


// var ajax = {
//     'saludar_mundo' : 'Hola ajax'
// }
"use strict";
"use strict";

var tablaVista = {}; //Json de los elementos y funciones de Tabla

(function ($, globalElements) {

    /* Llamamos al archivo elements.js */
    var elements = globalElements.getElements();

    /**
     * Funcionalidad que inicia la Tabla del plugin DataTable
     */
    this.initTabla = function () {
        /* Ruta a la acción que pinta la tabla */
        var tablaAjax = Routing.generate('datos_tabla');

        /* Configuración de la tabla */
        var options = {
            /* Configuración de columnas de la Tabla */
            "columns": [{ "data": "nombre" }, { "data": "posicion" }, { "data": "oficio" }, { "data": "edad" }, { "data": "fecha" }, { "data": "salario" }],
            /* Responsiva */
            "responsive": true,
            /* Scroll de la tabla */
            "scrollY": false,
            "scrollX": true,
            /* Tabla de lado del servidor AJAX */
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: tablaAjax,
                type: 'POST'
            },
            initComplete: function initComplete() {
                filtroTabla(); /* Filtros de la tabla */
                filt(); /* Select */
            }
        };

        $(elements.tabla.tbVista).DataTable(options); //Iniciar la tabla .DataTable
    };

    /* Funcionalidad para manipular los filtros */
    function filtroTabla() {

        /* Rastreamos cada columna de la tabla */
        $(elements.tabla.tbVista + ' thead th').each(function () {
            var title = $(this).text();
            /* Dibujamos los Filtros en cada columna */
            var plantillaFilt = $(this).html("\n                                                <div class=\"input-group\" data-name=\"" + title + "\">\n                                                    <input type=\"search\" class=\"form-control\" placeholder=\"Search " + title + "\" aria-describedby=\"basic-addon1\">\n                                                    <span class=\"input-group-btn\" id=\"span_selectFiltro\">\n                                                        <a href=\"#\" class=\"btn btn-primary btn-Select\">\n                                                            <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>\n                                                        </a>\n                                                    </span>\n                                                </div>\n                                            ");

            var filto = $('#example_wrapper > div.dataTables_scroll > div.dataTables_scrollHead > div > table > thead').append(plantillaFilt);

            filto;
        });

        /* Borrar la clase de sort asc/desc en el tr de filtros */
        $('#example_wrapper > div.dataTables_scroll > div.dataTables_scrollHead > div > table > thead > th').removeClass();
    };

    /* Funcionalidad que mostrara el select de la columna seleccionada */
    function filt() {
        $('.btn-Select').on('click', function () {
            console.log('click');
        });
    }
}).apply(tablaVista, [jQuery, globalElements]);
'use strict';

$(document).ready(function () {
    /* Llamamos al archivo elements.js */
    var elements = globalElements.getElements();

    /* Ruta de la vista */
    var rutaVista = Routing.generate('homepage');

    /* Redirecciona a la vista que queremos */
    $.ajax({
        url: rutaVista,
        context: document.body
    }).done(function () {
        var vista = Routing.generate('vista_actividad');
        $(elements.contenedor.contenedorPrincipal).load(vista, function () {
            tablaVista.initTabla(); //Inicia la tabla de la vista
            reactivos.vistaReactivos(); //Boton para entrar a la vista de reactivos
        }); //Carga vista  
    });
});
'use strict';

var reactivos = {}; //Json de los elementos y funciones de Tabla

(function ($, globalElements) {

    /* Llamamos al archivo elements.js */
    var elements = globalElements.getElements();

    /**
     * Funcionalidad que inicia la Tabla del plugin DataTable
     */
    this.vistaReactivos = function () {
        $(elements.buttons.buttonInsertReactivo).on('click', function () {
            // let rutaReactivos   = Routing.generate('');

            /* Redireccionamiento a la vista para reactivos */
            var rutaReactivo = Routing.generate('reactivos');
            $(elements.contenedor.contenedorPrincipal).load(rutaReactivo, function () {
                console.log('hola');
            });
        });
    };
}).apply(reactivos, [jQuery, globalElements]);