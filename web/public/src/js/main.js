$(document).ready(function(){
    /* Llamamos al archivo elements.js */
    let elements    = globalElements.getElements();
    
    /* Ruta de la vista */
    let rutaVista       = Routing.generate('homepage');

    /* Redirecciona a la vista que queremos */
    $.ajax({
      url       : rutaVista,
      context   : document.body
    }).done(function() {
        let vista = Routing.generate('vista_actividad');
        $(elements.contenedor.contenedorPrincipal).load(vista, function(){
            tablaVista.initTabla(); //Inicia la tabla de la vista
            reactivos.vistaReactivos(); //Boton para entrar a la vista de reactivos
        }); //Carga vista  
    });
});