let globalElements = {}; //Json de elementos

(function(){
    /**
     * Definimos los elementos que vamos a utilizar de la aplicación
     */
    this.getElements = function(){
        return{
            contenedor  : {
                contenedorPrincipal         : '#contedor_principal'
            },
            inputs      : {
                inputActividad              : '#input-prueba'
            },
            buttons     : {
                buttonInsertReactivo        : '#btn-insertReac'
            },
            tabla       : {
                tbVista                     : '#example'
            }
        }
    }
}).apply(globalElements); //Aplicando todo los elementos al globalElements