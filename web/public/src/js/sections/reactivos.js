let reactivos = {}; //Json de los elementos y funciones de Tabla

(function($, globalElements) {

    /* Llamamos al archivo elements.js */
    let elements    = globalElements.getElements();

    /**
     * Funcionalidad que inicia la Tabla del plugin DataTable
     */
    this.vistaReactivos = function(){
        $(elements.buttons.buttonInsertReactivo).on('click', function(){
            // let rutaReactivos   = Routing.generate('');

            /* Redireccionamiento a la vista para reactivos */
            let rutaReactivo = Routing.generate('reactivos');
            $(elements.contenedor.contenedorPrincipal).load(rutaReactivo, function(){
                console.log('hola');
            });
        });
    };

}).apply(reactivos, [jQuery, globalElements]);