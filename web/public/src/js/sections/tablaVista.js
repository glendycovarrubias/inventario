let tablaVista = {}; //Json de los elementos y funciones de Tabla

(function($, globalElements) {

    /* Llamamos al archivo elements.js */
    let elements    = globalElements.getElements();

    /**
     * Funcionalidad que inicia la Tabla del plugin DataTable
     */
    this.initTabla = function(){
        /* Ruta a la acción que pinta la tabla */
        let tablaAjax        = Routing.generate('datos_tabla');

        /* Configuración de la tabla */
        let options = {
            /* Configuración de columnas de la Tabla */
            "columns": [
                {"data": "nombre"},
                {"data": "posicion"},
                {"data": "oficio"},
                {"data": "edad"},
                {"data": "fecha"},
                {"data": "salario"}
            ],
            /* Responsiva */ 
            "responsive": true,
            /* Scroll de la tabla */
            "scrollY"   : false,
            "scrollX"   : true,
            /* Tabla de lado del servidor AJAX */
            "processing": true,
            "serverSide": true,
            "ajax": {
                url : tablaAjax,
                type: 'POST'
            },
            initComplete: function() {
                filtroTabla(); /* Filtros de la tabla */
                filt(); /* Select */
            }
        };

        $(elements.tabla.tbVista).DataTable(options); //Iniciar la tabla .DataTable
    };

    /* Funcionalidad para manipular los filtros */
    function filtroTabla(){
        
        /* Rastreamos cada columna de la tabla */
        $(elements.tabla.tbVista + ' thead th').each(function(){
            let title         = $(this).text();
            /* Dibujamos los Filtros en cada columna */
            let plantillaFilt = $(this).html(`
                                                <div class="input-group" data-name="`+title+`">
                                                    <input type="search" class="form-control" placeholder="Search `+title+`" aria-describedby="basic-addon1">
                                                    <span class="input-group-btn" id="span_selectFiltro">
                                                        <a href="#" class="btn btn-primary btn-Select">
                                                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            `);

            let filto = $('#example_wrapper > div.dataTables_scroll > div.dataTables_scrollHead > div > table > thead').append(plantillaFilt);

            filto;
            
        });

        /* Borrar la clase de sort asc/desc en el tr de filtros */
        $('#example_wrapper > div.dataTables_scroll > div.dataTables_scrollHead > div > table > thead > th').removeClass(); 
    };

    /* Funcionalidad que mostrara el select de la columna seleccionada */
    function filt(){
        $('.btn-Select').on('click', function(){
            console.log('click');
        });
    }

}).apply(tablaVista, [jQuery, globalElements]);