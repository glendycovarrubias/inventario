<?php 

/* Basico */
// src/AppBundle/Command/CreateUserCommand.php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

//Personalizar tu estilo
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class CreateUserCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('app:create-user')

            // the short description shown while running "php app/console list"
            ->setDescription('Creates a new user.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create a user...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        // outputs a message followed by a "\n"
        
        //Color verde
        $output->writeln('<info>oooohhhh :O</info>');
        //Color rojo
        $output->writeln('<error>Error</error>');
        //Color amarrillo
        $output->writeln('<comment>Warning</comment>');
        
        // texto negro sobre fondo cían
        $output->writeln('<question>¿a?</question>');

        /* Definir tu propio color */
        $style = new OutputFormatterStyle('white', 'magenta', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $style);
        $output->writeln('<fire>foo</fire>');

        // outputs a message without adding a "\n" at the end of the line
        $output->write('You are about to ');
        $output->write('create a user.');
    }
}