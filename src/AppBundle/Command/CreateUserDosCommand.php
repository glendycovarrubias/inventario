<?php 
// ...
namespace AppBundle\Command;

// use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

//Personalizar tu estilo
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class CreateUserDosCommand extends ContainerAwareCommand
{
    // ...
    protected function configure()
    {
        $this
            // the name of the command (the part after "app/console")
            ->setName('app:create-userDos')

            // the short description shown while running "php app/console list"
            ->setDescription('Creates a new user.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create a user...')
            // configure an argument
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
            // ...
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // access the container using getContainer()
        $userManager = $this->getContainer()->get('app.model');
        $userManager->create($input->getArgument('username'));

        // Personalizado
        $style = new OutputFormatterStyle('white', 'magenta', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $style);

        $output->writeln('<info>User successfully generated!</info>');
        $output->writeln([
            '<fire>>>>>User Creator<<<<</fire>',
            '<comment>======*******======</comment>',
            '',
        ]);
    }
}