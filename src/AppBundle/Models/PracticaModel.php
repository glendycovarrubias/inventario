<?php

namespace AppBundle\Models;
use AppBundle\Models\Conexion;
use Doctrine\DBAL\Exception\TableNotFoundException;

class PracticaModel{
    public $conn;

    public function __construct(Conexion $conn)
    {
        $this->conn = $conn;
    }

    public function obtenerNombreCampo(){
        $con    = $this->conn->con();
        $sql    =   "
                        SELECT 
                            COLUMN_NAME
                        FROM
                            INFORMATION_SCHEMA.COLUMNS
                        WHERE
                            TABLE_NAME = 'tablaP'
                    ";
        $query  = $con->prepare($sql);
        $query->execute();
        $datosCamp  = $query->fetchAll();

        return $datosCamp;
    }

    /**
     * [obtenerTabla description]
     *
     * @author   Glendy Covarrubias <gcovarrubias@simetrical.com.mx>
     * date 2018-01-04
     * @version [1.0]
     * @return  [type] [Devuelve todo los registro de la tabla tablaP]
     */
    public function obtenerTabla()
    {   
        $con    = $this->conn->con();
        $sql    = "SELECT * FROM app_prueba.tablaP";
        $query  = $con->prepare($sql);
        $query->execute();
        $datos = $query->fetchAll();
        //$datos = $query->fetch();

        try{
            return $datos;
        }catch (TableNotFoundException $ex){
            $error = $ex;
            return $error;
        }  
    }

    /**
     * [serverBusqueda description]
     *
     * @author   Glendy Covarrubias <gcovarrubias@simetrical.com.mx>
     * date 2018-01-08
     * @version [1.0]
     * @param   [type] $where [Valor que se escribio en el input del buscador]
     * @return  [type]        [Cuando se hace la consulta del buscador, para poder mostrar el registros o (s)]
     */
    public function serverBusqueda($where)
    {
        $con    = $this->conn->con();
        $sql    = "SELECT * FROM app_prueba.tablaP WHERE".$where."";
        $query  = $con->prepare($sql);
        $query->execute();
        $datosBusqueda = $query->fetchAll();

        $resultRegistros = array();
        foreach ($datosBusqueda as $row ) {
            $row['salario']         = number_format($row['salario'], 2);
            $resultRegistros[]      = $row ;
        }

        try{
            return $resultRegistros;
        }catch (TableNotFoundException $ex){
            $error = $ex;
            return $error;
        }
    }

    /**
     * [serverBusquedaVacia description]
     *
     * @author   Glendy Covarrubias <gcovarrubias@simetrical.com.mx>
     * date 2018-01-08
     * @version [1.0]
     * @return  [type] [Para mostrar los datos de la tabla sin niguna busqueda]
     */
    public function serverBusquedaVacia($orderBy, $orderType, $start, $length)
    {
        $con    = $this->conn->con(); /* Conexión a la BD */
        $sql    = "SELECT * FROM app_prueba.tablaP ORDER BY ".$orderBy." ".$orderType." LIMIT ".$start.",".$length." "; /* Query con los parametros */
        $query  = $con->prepare($sql); /* Preparo la consulta */
        $query->execute(); /* Y se ejecuta */
        $resultBusquedaVacia = $query->fetchAll(); /* Traigo todos los resultados */ 

        $data = array();
        foreach ($resultBusquedaVacia as $row ){
            $row['salario']         = number_format($row['salario'], 2);
            $data[]                 = $row ;
        }

        try{
            return $data;
        }catch(TableNotFoundException $ex){
            $error = $ex;
            return $error;
        }
    }

    /*Probando commands service*/
    public function create($nombre){
        $nombre = 'función create commands';
        return $nombre;
        // echo "hola";
        // print_r($nombre);
        // print_r('ahora todo tiene sentido XD');
        // exit;
    }

}