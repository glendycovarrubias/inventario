<?php
namespace AppBundle\Models;

use Doctrine\ORM\EntityManager;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

class Conexion
{

    public $em;
    public $query;

    public function __construct(EntityManager $entity)
    {
        $this->em   = $entity;
    }

    public function con()
    {
        $conn  = $this->em->getConnection();
        return $conn;
    }

    public function imprimirConsulta($sql, $datos = array())
    {
        $cadena = $sql;
        foreach ($datos as $campo => $valor) {
            $cadena = str_replace(":$campo", "'" . $valor . "'", $cadena);
        }
        echo $cadena;
    }
}
