<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Process\Exception\ProcessFailedException; //Para la importacion del inventario
use Symfony\Component\Finder\Finder; //Para buscar el archivo
use Symfony\Component\Filesystem\Filesystem; //Para decidir que hacer
use Symfony\Component\Process\Process; //Para la importacion del inventario

class DefaultController extends Controller
{
    /**
     * @Route("/", options={"expose"=true}, name="homepage")
     */
    public function indexAction(Request $request)
    {   
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/vista_actividad", options={"expose"=true}, name="vista_actividad")
     */
    public function vistaActividadAction(Request $request)
    {   
        $model      = $this->get('app.model');
        $resNomCamp = $model->obtenerNombreCampo();
        $resNomCamp = array_slice($resNomCamp, 1); 
    
        return $this->render('Actividad/Actividad.html.twig', compact('resNomCamp')); /* Redireccionamiento a la vista */
    }

    /**
     * @Route("/datosTabla", options={"expose"=true}, name="datos_tabla")
     */
    public function datosTablaAction(Request $request)
    {
        $model                  = $this->get('sim.model'); /* Llamamos al Modelo */
        $resultServer           = $model->obtenerTabla(); /* Resultados de la consulta total de registro */

        /* Parametros que devuelve DataTable $_POST del AJAX */
        /* Para garantizar que los Ajax regresen de las solicitudes de procesamiento del lado del servidor se dibujen en secuencia por DataTables */
        $draw                   = $_POST["draw"];
        $orderByColumnIndex     = $_POST['order'][0]['column'];// índice de la columna de clasificación (basado en 0 índice, es decir, 0 es el primer registro)
        $orderBy                = $_POST['columns'][$orderByColumnIndex]['data'];// Obtener el nombre de la columna de clasificación de su índice
        $orderType              = $_POST['order'][0]['dir']; // ASC or DESC
        $start                  = $_POST["start"];// Paginación del primer indicador de registro
        $length                 = $_POST['length'];// Número de registros que la tabla puede mostrar en el sorteo actual
        $recordsTotal           = count($resultServer);

        if(!empty($_POST['search']['value'])){
            /* Buscador */
            /* ESTO SUCEDE CUANDO TRAEMOS LA TABLA con el Search VALOR */
            for($i=0 ; $i<count($_POST['columns']);$i++){
                /* Obtenemos el nombre de cada columna usando su índice de la solicitud POST */
                $column             =   $_POST['columns'][$i]['data'];
                /* Tomamos el valor de la busqueda */
                $limpiarValorComa   = str_replace(',','',$_POST['search']['value']); //Quitamos Coma
                $limpiarValorPeso   = str_replace('$ ','', $limpiarValorComa); //Quitamos $ y espacio
                $where[]            = "$column like '%".$limpiarValorPeso."%'";             
            }
            
            $where                  = " ".implode(" OR " , $where); //id como 'searchValue' o nombre como 'searchValue'
            $resultServerBusqueda   = $model->serverBusqueda($where); /* Busqueda del registro */
            
            /* Consulta SQL para búsqueda con cláusulas limit y orderBy */
            $recordsFiltered    = count($resultServerBusqueda);//Recuento del resultado de la búsqueda            
            $resultServer       = $resultServerBusqueda; /* Resultado de los datos de la tabla cuando existe Busqueda */
            /* Fin de la consulta SQL y Resultado para el Buscador */
        }else{ /* ESTO SUCEDE CUANDO TRAEMOS LA TABLA con el Search Vacio */
            /* Consulta SQL para búsqueda con cláusulas limit y orderBy  -- Cargar todos los datos sin busqueda -- */
            $resultBusquedaTb   = $model->serverBusquedaVacia($orderBy, $orderType, $start, $length);

            /* Resultado de los datos de la tabla cuando no existe Busqueda */
            $resultServer       = $resultBusquedaTb;
            $recordsFiltered    = $recordsTotal;
        }

        /* Esto son los parametros que nececita Datable para Mostrar los Datos */
        $response = array(
            "draw"              => intval($draw),
            "recordsTotal"      => intval($recordsTotal), //Total de Datos Origen
            "recordsFiltered"   => intval($recordsFiltered), //Total de Datos Filtrado (Paginación)
            "data"              => $resultServer //Datos de la Tabla
        );

        $respuestatb = json_encode($response);
        print_r($respuestatb); //Pinta los datos de la tabla (Importante no Borrar)
        exit; //Se queda (Importante no Borrar) ya que ayuda a pintar la tabla
    }

    /**
     * [reactivosAction description]
     *
     * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
     * date 2018-01-10
     * @version [1.0]
     * @param   Request $request [description]
     * @return  [type]           [description]
     * @Route("/reactivosV", options={"expose"=true}, name="reactivos")
     */
    public function reactivosAction(Request $request){
        print_r('hola');
        exit;
    }
}
